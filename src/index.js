export const slugger = (args) =>{
    const slugged =  args.split(' ').reduce((acc,element)=>{
        return acc += (element + '-');
    },'');
    const result = slugged.slice(0,-1);
    return result;
}